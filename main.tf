module "backup" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-backup-cronjob.git?ref=v1.0.0"

  name                 = "db-${var.database.database}"
  namespace            = var.namespace
  rsync_private_key    = var.rsync_private_key
  rsync_username       = var.rsync_username
  source_configuration = <<EOT
driver=postgresql
address=${var.database.host}:5432
username=${var.database.username}
password=${var.database.password}
database=${var.database.database}
EOT
}
